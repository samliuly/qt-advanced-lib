## Qt Advanced Libraries

**欢迎邮件交流技术问题：1552883346@qq.com**

<img src="https://svg.hamm.cn/badge.svg?key=lang&value=C%2B%2B&color=44C07D"/>
<img src="https://svg.hamm.cn/badge.svg?key=version&value=Qt5.14&color=1182C2"/>
<img src="https://svg.hamm.cn/badge.svg?key=platform&value=windows&color=EC4758"/>
<img src="https://svg.hamm.cn/badge.svg?key=license&value=LGPL-3.0&color=F38D30"/>

本项目是一个Windows平台下可复用Qt C++模块的合集，其包含了众多经过大量测试以及实际应用的功能模块。本项目的开发目标是为了补充以及增强Qt对Windows平台的支持，分享Qt在Windows平台下开发时解决某些问题的可复用的方案。

### 1 QtWindows
基于Qt框架封装的Windows API扩展模块。  
文档：[QtWindows 模块说明](https://gitee.com/chen-jianli/qt-advanced-lib/tree/master/QtWindows)

### 2 QtMfcMixer
用于实现Win32/MFC框架与Qt框架混合开发的功能模块。   
文档：[QtMfcMixer 模块说明](https://gitee.com/chen-jianli/qt-advanced-lib/tree/master/QtMfcMixer)

### 3 QtFlatWindow
Qt Widget以及Qt Quick环境下可用的无边框、可伸缩、可拖动、可自定义标题栏的窗口控件。  
文档：[QtFlatWindow 模块说明](https://gitee.com/chen-jianli/qt-advanced-lib/tree/master/QtFlatWindow)

### 4 QtService
Windows服务程序开发模块。  
文档：[QtService 模块说明](https://gitee.com/chen-jianli/qt-advanced-lib/tree/master/QtService)

### 5 QtConfig
通用的日志和配置文件处理模块。  
文档：[QtConfig 模块说明](https://gitee.com/chen-jianli/qt-advanced-lib/tree/master/QtConfig)

### 6 QtMqtt
Mqtt网络通信的客户端开发模块。    
注意：本项目基于Qt5.14版本发布的QtMQTT模块改进。    
文档：[https://doc.qt.io/QtMQTT/index.html](https://doc.qt.io/QtMQTT/index.html)

