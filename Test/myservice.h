﻿#ifndef MYSERVICE_H
#define MYSERVICE_H

#include "qtservice.h"

class MyService:public QtService
{
public:
    MyService(int &argc, char **argv,
              QString serviceName, QString serviceDescription = "",
              QObject *parent = nullptr);
protected:
    void start() override;
    void stop() override;
};

#endif // MYSERVICE_H
