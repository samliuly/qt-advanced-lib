:: 服务程序exe所在路径, 注意=号前后不能有空格
@set exePath=D:\9.git_repository\Qt-Advanced-Lib\build\release\test.exe


 :: 服务状态
@ %exePath%  -v

@echo.

 :: 结束服务
 @ %exePath%  -t
 :: 卸载服务
 @ %exePath% -u
 :: 注册服务
 @ %exePath% -i
 :: 启动服务
 @ %exePath%

@pause