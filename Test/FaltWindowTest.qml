﻿import QtQuick 2.12
import QtQuick.Window 2.12
import Qt.FlatWindow 1.0


Window {
    width: 900
    height: 600
    title: "FlatWindow"
    flags: Qt.Window | Qt.FramelessWindowHint
    visible: true

    //无边框处理器
    QQuickFlatWindowHandler{
        id:flatHandler
        topDraggableMargin: 5
        leftDraggableMargin: 5
        rightDraggableMargin: 5
        bottomDraggableMargin: 5
        dragableHeight: 30
    }

}
