﻿#include <QApplication>
#include <QDebug>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QDir>
#include <QStandardPaths>
#include <QWidget>

#include "qwindowsmanager.h"
#include "qwindowsperformance.h"
#include "qquickflatwindowhandler.h"
#include "myservice.h"
#include "qtservice.h"
#include "qlogger.h"
#include "qconfiger.h"

int main(int argc, char *argv[])
{

/**************************** QtWindows Test ***********************************

    QApplication app(argc, argv);

    qDebug()<<"\n-----------System Info------------\n"
            <<"hostName:"<<QWindowsManager::hostName()<<"\n"
            <<"userName:"<<QWindowsManager::userName()<<"\n"
            <<"lanIp:"<<QWindowsManager::lanIp()<<"\n"
            <<"wanIp:"<<QWindowsManager::wanIp()<<"\n"
            <<"mac:"<<QWindowsManager::mac()<<"\n"
            <<"-----------System Info------------\n";


    qDebug()<<"\n----------- Registry ------------\n"
            <<"Create fileAss .kk to QtCreator:"<<QWindowsManager::createFileAssociation(".kk","QtProject.QtCreator.cpp")<<"\n"
            <<"Create contextMenu RunCmd:"<<QWindowsManager::createContextMenuItem("RunCmd","Run Cmd","cmd.exe","cmd.exe");
    QWindowsManager::setAutoStartEnabled("DD","cmd.exe");
    qDebug()<<" Set cmd.exe autoStart:"<<QWindowsManager::autoStartEnabled("DD");
    QWindowsManager::setAutoStartEnabled("DD","",false);
    QWindowsManager::setEnvironmentVariableValue("TESTENV",QStringList()<<"a"<<"b");
    QWindowsManager::appendEnvironmentVariableValue("TESTENV",QStringList()<<"c"<<"d");
    qDebug()<<" Set EnvVariable TESTENV:"<<QWindowsManager::environmentVariableValue("TESTENV");
    qDebug()<<"----------- Registry ------------\n";


    QWindowsPerformance sysPdh;
    qDebug()<<"\n----------- Performance ------------\n"
            <<"CPU UsgeRate:"<<sysPdh.performance(QWindowsPerformance::CPUUsageRate)<<" %\n"
            <<"CPU RunTime:"<<sysPdh.performance(QWindowsPerformance::CPURunTime)<<" s\n"
            <<"MEM UsgeRate:"<<sysPdh.performance(QWindowsPerformance::MemoryUsageRate)<<" %\n"
            <<"MEM UsageAmount:"<<sysPdh.performance(QWindowsPerformance::MemoryUsageAmount)<<" mb\n"
            <<"NET DownSpeed:"<<sysPdh.performance(QWindowsPerformance::NetworkDownloadSpeed)<<" kb/s\n"
            <<"NET UpSpeed:"<<sysPdh.performance(QWindowsPerformance::NetworkUploadSpeed)<<" kb/s";

    sysPdh.addMonitoringPerformance(QWindowsPerformance::CPUUsageRate);
    sysPdh.addMonitoringPerformance(QWindowsPerformance::CPURunTime);
    sysPdh.addMonitoringPerformance(QWindowsPerformance::MemoryUsageRate);
    sysPdh.addMonitoringPerformance(QWindowsPerformance::MemoryUsageAmount);
    sysPdh.addMonitoringPerformance(QWindowsPerformance::NetworkDownloadSpeed);
    sysPdh.addMonitoringPerformance(QWindowsPerformance::NetworkUploadSpeed);
    qDebug()<<"\nStart ASync Monitor:"<<sysPdh.startMonitor();
    qDebug()<<"----------- Performance ------------\n";

    return app.exec();

****************************** QtWindows Test **********************************/


/**************************** QtFlatWindow Test ********************************

    QApplication app(argc, argv);

    //Used in Qml
    qmlRegisterType<QQuickFlatWindowHandler>("Qt.FlatWindow", 1, 0, "QQuickFlatWindowHandler");
    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/FaltWindowTest.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();

**************************** QtFlatWindow Test ***********************************/


/**************************** QtService Test ************************************
    MyService service(argc,argv,"MyService","MyService Demo");
    return service.exec();
**************************** QtService Test ************************************/


/**************************** QtConfig Test ************************************
    QApplication app(argc,argv);
    QQmlApplicationEngine engine;

    QLogger::load();
    QLogger::setLogFileFullPath(QStandardPaths::writableLocation(QStandardPaths::DesktopLocation)+ "/log.txt");
    QLogger::setMsgContextVisible(QLogger::AlwaysShow);

    qDebug()<<"C++ Debug Message";
    qWarning()<<"C++ Warning Message";
    qCritical()<<"C++ Critical Message";
    qInfo()<<"C++ Info Message";

    const QUrl url(QStringLiteral("qrc:/QmlLogTest.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);
    QLogger::unload();


    QString path =QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
    QJsonObject value;
    value["key1"] = "value1";
    value["key2"] = "value2";

    QConfiger configer(path);
    qDebug()<<"write app.json :"<<configer.writeConfigFile("app.json",value);
    qDebug()<<"read app.json :"<<configer.readConfigFile("/app.json");
    qDebug()<<"rename app2.json :"<<configer.renameConfigFile("app.json","app2.json");
    qDebug()<<"remove app2.json :"<<configer.removeConfigFile("app2.json");

    return app.exec();
**************************** QtConfig Test ************************************/
}
