﻿import QtQuick 2.12
import QtQuick.Window 2.12

Window {
    width: 900
    height: 600
    title: "QmlLogTest"
    visible: false

    Component.onCompleted: {
        console.log("Qml Debug Message")
        console.warn("Qml Warning Message")
        console.error("Qml Critical Message")
        console.info("Qml Info Message")
    }

}
