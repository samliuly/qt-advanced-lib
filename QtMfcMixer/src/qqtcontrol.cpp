#ifdef QT3_SUPPORT
#undef QT3_SUPPORT
#endif

#ifdef UNICODE
#undef UNICODE
#endif

#include "..\include\qqtcontrol.h"
#include "..\include\qmfcapp.h"

#ifdef QTMFCMIXER_WITHMFC
#include <afxwin.h>
#endif

#include <qevent.h>
#include <qt_windows.h>

#if QT_VERSION >= 0x050000
#include <QWindow>
#include <qpa/qplatformnativeinterface.h>
#define QT_WA(unicode, ansi) unicode
#endif

/*!
    \class QQtControl QQtControl.h
    \brief The QQtControl class is a Qt widget that can be child of a
    native Win32 widget.

    The QQtControl class is the bridge between an existing application
    user interface developed using native Win32 APIs or toolkits like
    MFC, and Qt based GUI elements.

    Using QQtControl as the parent of QDialogs will ensure that
    modality, placement and stacking works properly throughout the
    entire application. If the child widget is a top level window that
    uses the \c WDestructiveClose flag, QQtControl will destroy itself
    when the child window closes down.

    Applications moving to Qt can use QQtControl to add new
    functionality, and gradually replace the existing interface.
*/

/*!
    Creates an instance of QQtControl. \a hParentWnd is the handle to
    the native Win32 parent. If a \a parent is provided the object is
    owned by that QObject. \a f is passed on to the QWidget constructor.
*/
QQtControl::QQtControl(HWND hParentWnd, QObject *parent, Qt::WindowFlags f)
: QWidget(0, f), hParent(hParentWnd), prevFocus(0), reenable_parent(false)
{
    if (parent)
        QObject::setParent(parent);

    init();
}

#ifdef QTMFCMIXER_WITHMFC
/*!
    \overload

    Creates an instance of QQtControl. \a parentWnd is a pointer to an
    MFC window object. If a \a parent is provided the object is owned
    by that QObject. \a f is passed on to the QWidget constructor.
*/
QQtControl::QQtControl(CWnd *parentWnd, QObject *parent, Qt::WindowFlags f)
: QWidget(0, f), hParent(parentWnd ? parentWnd->m_hWnd : 0), prevFocus(0), reenable_parent(false)
{
    if (parent)
        QObject::setParent(parent);

    init();
}
#endif


void QQtControl::init()
{
    Q_ASSERT(hParent);

    if (hParent) {
#if QT_VERSION >= 0x050000
        setProperty("_q_embedded_native_parent_handle", WId(hParent));
#endif
	// make the widget window style be WS_CHILD so SetParent will work
	QT_WA({
        SetWindowLong((HWND)winId(), GWL_STYLE, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS);
	}, {
        SetWindowLongA((HWND)winId(), GWL_STYLE, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS);
	})
#if QT_VERSION >= 0x050000
        QWindow *window = windowHandle();
        HWND h = static_cast<HWND>(QGuiApplication::platformNativeInterface()->
                                nativeResourceForWindow("handle", window));
        SetParent(h, hParent);
        window->setFlags(Qt::FramelessWindowHint);
#else
        SetParent(winId(), hParent);
#endif
        QEvent e(QEvent::EmbeddingControl);
        QApplication::sendEvent(this, &e);
    }
}

/*!
    Destroys this object, freeing all allocated resources.
*/
QQtControl::~QQtControl()
{
}

/*!
    Returns the handle of the native Win32 parent window.
*/
HWND QQtControl::parentWindow() const
{
    return hParent;
}

/*!
    \reimp
*/
void QQtControl::childEvent(QChildEvent *e)
{
    QObject *obj = e->child();
    if (obj->isWidgetType()) {
        if (e->added()) {
	    if (obj->isWidgetType()) {
	        obj->installEventFilter(this);
	    }
        } else if (e->removed() && reenable_parent) {
	    reenable_parent = false;
	    EnableWindow(hParent, true);
            obj->removeEventFilter(this);
        }
    }
    QWidget::childEvent(e);
}

/*! \internal */
void QQtControl::saveFocus()
{
    if (!prevFocus)
	prevFocus = ::GetFocus();
    if (!prevFocus)
	prevFocus = parentWindow();
}

/*!
    Shows this widget. Overrides QWidget::show().
    
    \sa showCentered()
*/
void QQtControl::show()
{
    saveFocus();
    QWidget::show();
}

/*!
    Centers this widget over the native parent window. Use this
    function to have Qt toplevel windows (i.e. dialogs) positioned
    correctly over their native parent windows.

    \code
    QQtControl qwin(hParent);
    qwin.center();

    QMessageBox::information(&qwin, "Caption", "Information Text");
    \endcode

    This will center the message box over the client area of hParent.
*/
void QQtControl::center()
{
    const QWidget *child = findChild<QWidget*>();
    if (child && !child->isWindow()) {
        qWarning("QQtControl::center: Call this function only for QWinWidgets with toplevel children");
    }
    RECT r;
    GetWindowRect(hParent, &r);
    setGeometry((r.right-r.left)/2+r.left, (r.bottom-r.top)/2+r.top,0,0);
}

/*!
    \obsolete

    Call center() instead.
*/
void QQtControl::showCentered()
{
    center();
    show();
}

/*!
    Sets the focus to the window that had the focus before this widget
    was shown, or if there was no previous window, sets the focus to
    the parent window.
*/
void QQtControl::resetFocus()
{
    if (prevFocus)
	::SetFocus(prevFocus);
    else
	::SetFocus(parentWindow());
}

/*! \reimp
*/
#if QT_VERSION >= 0x050000
bool QQtControl::nativeEvent(const QByteArray &, void *message, long *)
#else
bool QQtControl::winEvent(MSG *msg, long *)
#endif
{
#if QT_VERSION >= 0x050000
    MSG *msg = (MSG *)message;
#endif
    if (msg->message == WM_SETFOCUS) {
        Qt::FocusReason reason;
        if (::GetKeyState(VK_LBUTTON) < 0 || ::GetKeyState(VK_RBUTTON) < 0)
            reason = Qt::MouseFocusReason;
        else if (::GetKeyState(VK_SHIFT) < 0)
            reason = Qt::BacktabFocusReason;
        else
            reason = Qt::TabFocusReason;
        QFocusEvent e(QEvent::FocusIn, reason);
        QApplication::sendEvent(this, &e);
    }

    return false;
}

/*!
    \reimp
*/
bool QQtControl::eventFilter(QObject *o, QEvent *e)
{
    QWidget *w = (QWidget*)o;

    switch (e->type()) {
    case QEvent::WindowDeactivate:
	if (w->isModal() && w->isHidden())
	    BringWindowToTop(hParent);
	break;

    case QEvent::Hide:
	if (reenable_parent) {
	    EnableWindow(hParent, true);
	    reenable_parent = false;
	}
	resetFocus();
        if (w->testAttribute(Qt::WA_DeleteOnClose) && w->isWindow())
	    deleteLater();
	break;

    case QEvent::Show:
	if (w->isWindow()) {
	    saveFocus();
	    hide();
	    if (w->isModal() && !reenable_parent) {
		EnableWindow(hParent, false);
		reenable_parent = true;
	    }
	}
	break;

    case QEvent::Close:
    	::SetActiveWindow(hParent);
	if (w->testAttribute(Qt::WA_DeleteOnClose))
	    deleteLater();
	break;

    default:
	break;
    }
    
    return QWidget::eventFilter(o, e);
}

/*! \reimp
*/
void QQtControl::focusInEvent(QFocusEvent *e)
{
    QWidget *candidate = this;

    switch (e->reason()) {
    case Qt::TabFocusReason:
    case Qt::BacktabFocusReason:
        while (!(candidate->focusPolicy() & Qt::TabFocus)) {
            candidate = candidate->nextInFocusChain();
            if (candidate == this) {
                candidate = 0;
                break;
            }
        }
        if (candidate) {
            candidate->setFocus(e->reason());
            if (e->reason() == Qt::BacktabFocusReason || e->reason() == Qt::TabFocusReason) {
                candidate->setAttribute(Qt::WA_KeyboardFocusChange);
                candidate->window()->setAttribute(Qt::WA_KeyboardFocusChange);
            }
            if (e->reason() == Qt::BacktabFocusReason)
                QWidget::focusNextPrevChild(false);
        }
        break;
    default:
        break;
    }
}

/*! \reimp
*/
bool QQtControl::focusNextPrevChild(bool next)
{
    QWidget *curFocus = focusWidget();
    if (!next) {
        if (!curFocus->isWindow()) {
            QWidget *nextFocus = curFocus->nextInFocusChain();
            QWidget *prevFocus = 0;
            QWidget *topLevel = 0;
            while (nextFocus != curFocus) {
                if (nextFocus->focusPolicy() & Qt::TabFocus) {
                    prevFocus = nextFocus;
                    topLevel = 0;
                } else if (nextFocus->isWindow()) {
                    topLevel = nextFocus;
                }
                nextFocus = nextFocus->nextInFocusChain();
            }

            if (!topLevel) {
                return QWidget::focusNextPrevChild(false);
            }
        }
    } else {
        QWidget *nextFocus = curFocus;
        while (1) {
            nextFocus = nextFocus->nextInFocusChain();
            if (nextFocus->isWindow())
                break;
            if (nextFocus->focusPolicy() & Qt::TabFocus) {
                return QWidget::focusNextPrevChild(true);
            }
        }
    }

    ::SetFocus(hParent);

    return true;
}
