#ifndef QWINWIDGET_H
#define QWINWIDGET_H

#include <QWidget>

#if defined(QTMFCMIXER_LIBRARY)
#  define QTMFCMIXER_EXPORT __declspec(dllexport)
#else
#  define QTMFCMIXER_EXPORT __declspec(dllimport)
#endif

#if defined(_AFXDLL) && defined(_MSC_VER)
#define QTMFCMIXER_WITHMFC
class CWinApp;
#endif

class CWnd;

/*
    QQtControl用于将Qt控件集成到MFC中使用。
*/
class QTMFCMIXER_EXPORT QQtControl : public QWidget
{
    Q_OBJECT
public:
    QQtControl( HWND hParentWnd, QObject *parent = 0, Qt::WindowFlags f = 0 );
#ifdef QTMFCMIXER_WITHMFC
    QQtControl( CWnd *parnetWnd, QObject *parent = 0, Qt::WindowFlags f = 0 );
#endif
    ~QQtControl();

    void show();
    void center();
    void showCentered();

    HWND parentWindow() const;

protected:
    void childEvent( QChildEvent *e );
    bool eventFilter( QObject *o, QEvent *e );

    bool focusNextPrevChild(bool next);
    void focusInEvent(QFocusEvent *e);
#if QT_VERSION >= 0x050000
    bool nativeEvent(const QByteArray &eventType, void *message, long *result);
#else
    bool winEvent(MSG *msg, long *result);
#endif

private:
    void init();

    void saveFocus();
    void resetFocus();

    HWND hParent;
    HWND prevFocus;
    bool reenable_parent;
};

#endif // QWINWIDGET_H
