#ifndef QWINHOST_H
#define QWINHOST_H

#include <QtWidgets/QWidget>

#if defined(QTMFCMIXER_LIBRARY)
#  define QTMFCMIXER_EXPORT __declspec(dllexport)
#else
#  define QTMFCMIXER_EXPORT __declspec(dllimport)
#endif

#if defined(_AFXDLL) && defined(_MSC_VER)
#define QTMFCMIXER_WITHMFC
class CWinApp;
#endif

/*
    QMfcControl用于将MFC控件集成到Qt中使用。

    QMfcControl是win32或MFC控件的包装器，它将原生的win32
    或MFC控件封装成QWidget控件，使得它们能够像Qt原生的控件
    一样使用。
*/
class QTMFCMIXER_EXPORT QMfcControl : public QWidget
{
    Q_OBJECT
public:
    QMfcControl(QWidget *parent = 0, Qt::WindowFlags f = 0);
    ~QMfcControl();

    void setWindow(HWND);
    HWND window() const;

protected:
    virtual HWND createWindow(HWND parent, HINSTANCE instance);

    bool event(QEvent *e);
    void showEvent(QShowEvent *);
    void focusInEvent(QFocusEvent*);
    void resizeEvent(QResizeEvent*);

#if QT_VERSION >= 0x050000
    bool nativeEvent(const QByteArray &eventType, void *message, long *result);
#else
    bool winEvent(MSG *msg, long *result);
#endif

private:
    void fixParent();
    friend void* getWindowProc(QMfcControl*);

    void *wndproc;
    bool own_hwnd;
    HWND hwnd;
};

#endif
