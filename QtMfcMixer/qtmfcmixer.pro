QT += core widgets gui-private

TEMPLATE = lib
LIBS += -luser32

DEFINES *= _AFXDLL
DEFINES += QTMFCMIXER_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

CONFIG(debug, debug|release){
    TARGET = qmfcmixerd
}else{
    TARGET = qmfcmixer
}

DESTDIR = ../lib

SOURCES += \
    src/qmfcapp.cpp \
    src/qmfccontrol.cpp \
    src/qqtcontrol.cpp

HEADERS += \
    include/qmfcapp.h \
    include/qmfccontrol.h \
    include/qqtcontrol.h

unix {
    target.path = /usr/lib
}
