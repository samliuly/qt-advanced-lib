## QtMfcMixer

### 1 介绍
QtMfcMixer是一个用于实现Win32/MFC与Qt进行混合开发的框架。

### 2 编译
**环境：** windows、vs2010+、 Qt5+   
**步骤：**
- 在QtCreator中打开qtmfcmixer.pro，执行编译。
- 将/include目录下的头文件包含到你的项目中。
- 将/lib目录下的.lib文件链接到你的项目中。
- 将/lib目录下的.dll文件复制到你的可执行程序目录下。

### 3 功能
#### （1）在Win32/MFC中调用Qt API    
对于非GUI对象（例如QString,QFile等），在Win32/MFC项目中动态链接Qt后直接调用即可。而对于GUI对象（例如QWidget等），由于它们依赖于Qt的事件循环系统，因此还需要使用Qt的事件循环来替代Win32/MFC本身的事件循环，才能够使它们正常工作。以MFC程序为例：
```
BOOL CMyWinApp::InitInstance()
{
    // MFC init.... 

    //必须在任何GUI对象创建之前初始化QMfcApp
    QMfcApp::instance(this);  

    // Qt init... 

    QMyDialog dlg;
    dlg.exec();
}

int CMyWinApp::Run()
{
    //使用Qt的事件循环替代MFC的事件循环
    return QMfcApp::run(this);
}
```

#### （2）在Win32/MFC中调用基于Qt的Dll    
在MFC中调用Qt Dll与(1)的情况稍有不同，这是因为多个Qt Dll可能存在多个QApplication对象，为了使不同Dll的之间的事件循环不受影响，此时应采用拦截Win32/MFC主程序消息的方式，将Win32/MFC消息转入到Qt Dll中执行。例如：
```
//Qt Dll的pro关键配置
TEMPLATE = lib
CONFIG += dll

//Qt Dll的main函数
BOOL WINAPI DllMain( HINSTANCE hInstance, DWORD dwReason, LPVOID lpvReserved)
{
    static bool ownApplication = FALSE;

    if ( dwReason == DLL_PROCESS_ATTACH )
    {
        //Dll被加载时，通过Hook拦截MFC消息
        ownApplication = QMfcApp::pluginInstance( hInstance );
    }   
    else if ( dwReason == DLL_PROCESS_DETACH &&ownApplication )
    {
        //Dll被卸载时，销毁QApplication对象，取消Hook
        delete qApp;
    }
    
    return TRUE;
}

//Qt Dll的导出函数
extern "C" __declspec(dllexport) void showMyDialog( HWND parent )
{
    QMyDialog dlg;
    dlg.exec();
}
```

#### （3）在Win32/MFC窗口中显示Qt控件
- 采用（1）的方法用Qt的事件循环替换Win32/MFC的事件循环。
- 继承QQtControl，将所要提供给MFC窗口显示的Qt控件封装到此类中。
- 在MFC窗口中创建QQtControl的实例，并将其句柄传递给QQtControl作为其父窗口。

#### （4）在Qt窗口中显示Win32/MFC控件
- 继承QMfcControl，将Win32/MFC控件的句柄通过setWindow()传递给QMfcControl，或直接重写createWindow()创建Win32/MFC控件。
- 在Qt窗口中创建QMfcControl的实例，并将其作为父对象传递给QMfcControl。

### 4 Q&A
#### （1）如何在Win32/MFC中动态链接Qt  
- 在本地计算机上安装Qt。注意Qt的版本和位数必须与你MFC项目所使用的VC版本和位数相兼容。
- 在Win32/MFC项目的 “ 属性 - C/C++ - 附加包含目录 ” 设置中加入Qt库的头文件目录（例如C:\Qt5.14.2\5.14.2\msvc2017_64\include）。
- 在Win32/MFC项目的 “ 属性 - 链接器 - 通用 - 附加库目录 ” 设置中加入Qt的lib库目录（例如C:\Qt5.14.2\5.14.2\msvc2017_64\lib）
- 在Win32/MFC项目的 “ 属性 - 链接器 - 输入 - 附加依赖 ” 设置中加入依赖的Qt lib文件（例如Qt5Core.lib;Qt5Gui.lib;Qt5Widgets.lib）。    
ps：一般的，调用了Qt的哪个模块就需要添加对应的lib文件。否则编译时将会出现“无法解析的外部符号...”等错误。
- 在需要调用Qt API的地方，包含该API对应的头文件即可。注意需要加上该API所在的模块目录（例如#include <QtCore/qfile.h>）。

#### （2）如何在Win32/MFC中调用Qml
- 方法一：采用“在Win32/MFC中调用Qt API”的方法替换事件循环后，直接在Win32/MFC中使用QtQuickView或QtQuickWidget加载本地的Qml文件即可。如果Qml界面需要调用Win32/MFC中的函数时，则可以将它们封装在QObject对象中，然后在通过注册Qml上下文属性暴露给Qml调用即可。
- 方法二：采用“在Win32/MFC中调用基于Qt的Dll”的方法将Qt Qml界面封装在Qt Dll中，然后导出函数给Win32/MFC调用即可，这与一般的Qml开发并无本质区别。如果Qt Dll中的Qml界面需要调用Win32/MFC中的函数时，则可以将它们作为全局函数指针传递给Qt Dll，然后在通过注册Qml上下文属性暴露给Qml调用即可。

#### （3）如何在Win32/MFC中调用Qt资源
由于Qt使用了不同于Win32/MFC的资源系统，因此Qt和Win32/MFC无法直接相互调取资源。一般情况下，建议Qt使用的资源存放在Qt Dll项目中，Win32/MFC使用的资源存放在Win32/MFC项目中，这样一来两个框架都能够使用各自原生的资源系统处理其资源。但如果确实需要在Win32/MFC项目中访问Qt资源，则可按照以下方式处理：
- 创建qrc文件，并将Qt资源添加到qrc文件中。
- 执行以下命令行，编译qrc资源文件：
```
rcc -binary myres.qrc -o myres.rcc 

ps：rcc是位于Qt库目录下的rcc.exe程序。将Qt库目录添加到系统环境变量后即可直接在命令行中调用它。
```
- 在Win32/MFC程序初始化时，注册资源文件：
```
#include <QResource> 

BOOL CMyWinApp::InitInstance()
{
    // .... 

    // C:/path/myres.rcc是编译后的资源文件的位置，应根据实际情况指定
    QResource::registerResource("C:/path/myres.rcc");

    // ... 
}
```