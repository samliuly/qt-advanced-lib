﻿#ifndef NATIVEWINDOWHELPER_P_H
#define NATIVEWINDOWHELPER_P_H

#include "qnativewindowhelper.h"

class QNativeWindowHelperPrivate
{
    Q_DECLARE_PUBLIC(QNativeWindowHelper)

public:
    QNativeWindowHelperPrivate();
    virtual ~QNativeWindowHelperPrivate();

public:
    void updateWindowStyle();
    int hitTest(int x, int y) const;
    bool isMaximized() const;
    QMargins draggableMargins() const;
    QMargins maximizedMargins() const;
    QRect availableGeometry() const;

    QWindow            *window;
    NativeWindowTester *tester;
    HWND oldWindow;
    qreal scaleFactor;

protected:
    QNativeWindowHelper *q_ptr;
};

#endif // NATIVEWINDOWHELPER_P_H
