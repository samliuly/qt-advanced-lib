﻿#include "qwidgetflatwindowhandler_p.h"
#include "qwidgetflatwindowhandler.h"

#include <windows.h>

// class QWidgetFlatWindowHandler

QWidgetFlatWindowHandler::QWidgetFlatWindowHandler(QWidget *parent)
    : QObject(parent)
    , d_ptr(new QWidgetFlatWindowHandlerPrivate())
{
    Q_D(QWidgetFlatWindowHandler);
    d->window = parent;
    Q_CHECK_PTR(parent);

    if (d->window)
        d->window->installEventFilter(this);
}

QWidgetFlatWindowHandler::~QWidgetFlatWindowHandler()
{
}

void QWidgetFlatWindowHandler::setDraggableMargins(int left, int top, int right, int bottom)
{
    Q_D(QWidgetFlatWindowHandler);

    d->priDraggableMargins = QMargins(left, top, right, bottom);
}

void QWidgetFlatWindowHandler::setMaximizedMargins(int left, int top, int right, int bottom)
{
    Q_D(QWidgetFlatWindowHandler);

    d->priMaximizedMargins = QMargins(left, top, right, bottom);
}

void QWidgetFlatWindowHandler::setDraggableMargins(const QMargins &margins)
{
    Q_D(QWidgetFlatWindowHandler);

    d->priDraggableMargins = margins;
}

void QWidgetFlatWindowHandler::setMaximizedMargins(const QMargins &margins)
{
    Q_D(QWidgetFlatWindowHandler);

    d->priMaximizedMargins = margins;
}

QMargins QWidgetFlatWindowHandler::draggableMargins() const
{
    Q_D(const QWidgetFlatWindowHandler);

    return d->priDraggableMargins;
}

QMargins QWidgetFlatWindowHandler::maximizedMargins() const
{
    Q_D(const QWidgetFlatWindowHandler);

    return d->priMaximizedMargins;
}

void QWidgetFlatWindowHandler::addIncludeItem(QWidget *item)
{
    Q_D(QWidgetFlatWindowHandler);

    d->includeItems.insert(item);
}

void QWidgetFlatWindowHandler::removeIncludeItem(QWidget *item)
{
    Q_D(QWidgetFlatWindowHandler);

    d->includeItems.remove(item);
}

void QWidgetFlatWindowHandler::addExcludeItem(QWidget *item)
{
    Q_D(QWidgetFlatWindowHandler);

    d->excludeItems.insert(item);
}

void QWidgetFlatWindowHandler::removeExcludeItem(QWidget *item)
{
    Q_D(QWidgetFlatWindowHandler);

    d->excludeItems.remove(item);
}

void QWidgetFlatWindowHandler::setTitleBarHeight(int value)
{
    Q_D(QWidgetFlatWindowHandler);

    if (value != d->titleBarHeight) {
        d->titleBarHeight = value;
        emit titleBarHeightChanged(value);
    }
}

int QWidgetFlatWindowHandler::titleBarHeight() const
{
    Q_D(const QWidgetFlatWindowHandler);

    return d->titleBarHeight;
}

qreal QWidgetFlatWindowHandler::scaleFactor() const
{
    Q_D(const QWidgetFlatWindowHandler);

    return d->helper ? d->helper->scaleFactor() : 1.0;
}

bool QWidgetFlatWindowHandler::isMaximized() const
{
    Q_D(const QWidgetFlatWindowHandler);

    return d->maximized;
}

void QWidgetFlatWindowHandler::minimizeWindow()
{
    Q_D(QWidgetFlatWindowHandler);

    if (d->window) {
        d->window->showMinimized();
    }
}

void QWidgetFlatWindowHandler::maximizeWindow()
{
    Q_D(QWidgetFlatWindowHandler);

    if (d->window) {
        if (d->window->windowState() & Qt::WindowMaximized) {
            d->window->showNormal();
        } else {
            d->window->showMaximized();
        }
    }
}

void QWidgetFlatWindowHandler::closeWindow()
{
    Q_D(QWidgetFlatWindowHandler);

    if (d->window) {
        d->window->close();
    }
}

bool QWidgetFlatWindowHandler::eventFilter(QObject *obj, QEvent *ev)
{
    Q_D(QWidgetFlatWindowHandler);

    if (ev->type() == QEvent::WindowStateChange) {
        bool maximized = d->window->windowState() & Qt::WindowMaximized;
        if (maximized != d->maximized) {
            d->maximized = maximized;
            emit maximizedChanged(maximized);
        }
    } else if (ev->type() == QEvent::WinIdChange) {
        if (nullptr == d->helper) {
            auto w = d->window->windowHandle();

            d->helper = new QNativeWindowHelper(w, d);
            connect(d->helper, &QNativeWindowHelper::scaleFactorChanged,
                    this, &QWidgetFlatWindowHandler::scaleFactorChanged);
            if (!qFuzzyCompare(d->helper->scaleFactor(), 1.0)) {
                emit scaleFactorChanged(d->helper->scaleFactor());
            }
        }
    }

#if QT_VERSION < QT_VERSION_CHECK(5, 9, 0)
    if ((ev->type() == QEvent::Resize) || (ev->type() == QEvent::WindowStateChange)) {
        if (d->window->windowState() & Qt::WindowMaximized) {
            const QMargins &m = d->priMaximizedMargins;
            int r = GetSystemMetrics(SM_CXFRAME) * 2 - m.left() - m.right();
            int b = GetSystemMetrics(SM_CYFRAME) * 2 - m.top() - m.bottom();

            d->window->setContentsMargins(0, 0, r, b);
        } else {
            d->window->setContentsMargins(0 ,0, 0, 0);
        }
    }
#endif

    return QObject::eventFilter(obj, ev);
}

// class QWidgetFlatWindowHandlerPrivate

QWidgetFlatWindowHandlerPrivate::QWidgetFlatWindowHandlerPrivate()
    : window(nullptr)
    , helper(nullptr)
    , titleBarHeight(0)
    , maximized(false)
{
}

QWidgetFlatWindowHandlerPrivate::~QWidgetFlatWindowHandlerPrivate()
{
}

QMargins QWidgetFlatWindowHandlerPrivate::draggableMargins() const
{
    return priDraggableMargins;
}

QMargins QWidgetFlatWindowHandlerPrivate::maximizedMargins() const
{
    return priMaximizedMargins;
}

bool QWidgetFlatWindowHandlerPrivate::hitTest(const QPoint &pos) const
{
    int scaledTitleBarHeight = titleBarHeight * helper->scaleFactor();

    if (!window)
        return false;
    else if (scaledTitleBarHeight == 0)
        return false;
    else if ((scaledTitleBarHeight > 0)
             && (pos.y() >= scaledTitleBarHeight))
        return false;

    int currentIndex = -1;
    QWidget *items[32] = {0};
    auto child = window;
    QPoint p = pos;

    while (child && (currentIndex < 31)) {
        items[++currentIndex] = child;
        auto grandchild = child->childAt(p);
        if (nullptr == grandchild) {
            break;
        }

        p = grandchild->mapFrom(child, p);
        child = grandchild;
    }

    while (currentIndex > 0) {
        child = items[currentIndex];
        --currentIndex;

        if (includeItems.contains(child)) {
            break;
        } else if (excludeItems.contains(child)) {
            return false;
        } else if (window == child) {
            break;
        }
    }

    return true;
}
