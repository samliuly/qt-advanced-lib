﻿#ifndef NATIVEWINDOWHELPER_H
#define NATIVEWINDOWHELPER_H

#include <QPoint>
#include <QWindow>
#include <QMargins>

class QNativeWindowHelperPrivate;

/*
    定义可移动区域的辅助类
*/
class NativeWindowTester
{
public:
    virtual QMargins draggableMargins() const = 0;
    virtual QMargins maximizedMargins() const = 0;
    virtual bool hitTest(const QPoint &pos) const = 0;
};

/*
    处理窗口移动及缩放的辅助类
*/
class QNativeWindowHelper : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QNativeWindowHelper)

public:
    QNativeWindowHelper(QWindow *window, NativeWindowTester *tester);
    explicit QNativeWindowHelper(QWindow *window);
    ~QNativeWindowHelper();

    bool nativeEventFilter(void *msg, long *result);

    qreal scaleFactor() const;

protected:
    bool eventFilter(QObject *obj, QEvent *ev) final;
    QScopedPointer<QNativeWindowHelperPrivate> d_ptr;

signals:
    void scaleFactorChanged(qreal factor);

};

#endif // NATIVEWINDOWHELPER_H
