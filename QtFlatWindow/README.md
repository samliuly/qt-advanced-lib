## QtFlatWindow

### 1 介绍
QtFlatWindow是一个可用于Qt Widget以及Qt Quick的窗口控件，其完美的实现了一种无边框、可拖动、可伸缩的窗口，并且保留了底层Windows窗口的所有特性。


### 2 使用
用例参见QtFlatWindow/example/example.pro，以下简述本模块集成方法：

（1）将QtFlatWindow模块的整个文件夹复制到项目的某个目录下。  

（2）在项目的.pro文件中使用下述命令导入模块的.pri文件：
```
# ../QtFlatWindow/qtflatwindow.pri是相对.pro文件的路径
include(../QtFlatWindow/qtflatwindow.pri)
```

（3）对项目执行qmake刷新文件依赖。

（4）如果需要在Qt Qml中使用，则需要先注册QQuickFlatWindowHandler类型然后再将QQuickFlatWindowHandler定义到Window或ApplicationWindow元素下即可，例如：
```
//main.cpp
#include "qquickflatwindowhandler.h"

int main(){
  //....

  qmlRegisterType<QQuickFlatWindowHandler>("Qt.FlatWindow", 1, 0, "QQuickFlatWindowHandler");

  //....
}


//main.qml
import QtQuick 2.12
import QtQuick.Window 2.12
import Qt.FlatWindow 1.0

Window {
    width: 900
    height: 600
    title: "FlatWindow"
    flags: Qt.Window | Qt.FramelessWindowHint
    visible: true
  
    QQuickFlatWindowHandler{
        topDraggableMargin: 5
        leftDraggableMargin: 5
        rightDraggableMargin: 5
        bottomDraggableMargin: 5
        dragableHeight: 30
    }
}
```
（5）如果需要在Qt C++中使用，则可直接在QWidget的构造函数中实例化一个QWidgetFlatWindow子对象即可，例如：
```
//mainwindow.cpp
MainWindow::MainWindow(QWidget *parent): 
    QWidget(parent, Qt::FramelessWindowHint), 
    ui(new Ui::MainWindow())
{
    ui->setupUi(this);

    setWindowTitle("FlatWindow");

    auto helper = new QWidgetFramelessWindowHandler(this);
    helper->setDraggableMargins(5, 5, 5, 5);
    helper->setMaximizedMargins(5, 5, 5, 5);
    helper->setTitleBarHeight(30);
}
```

### 3 API
#### 3.1 QQuickFlatWindow
此类实现了Qt Qml中Window的无边框窗口特性：
- 可通过top/bottom/left/rightDraggableMargin属性设置被识别的最小拖动距离。
- 可通过top/bottom/left/rightMaximizedMargin属性设置被识别的在桌面边缘进行最大化的距离。
- 可通过dragableHeight属性设置窗口顶部可拖动区域的高度。
- 可通过minimize/maximize/closeWindow()方法触发窗口的最小化、最大化/还原、关闭功能。
- 可通过addIncludeItem()/addExcludeItem()/addExcludeRect()方法设置不响应拖动事件的窗口的子控件或窗口中的区域。
  
#### 3.2 QWidgetFlatWindow
此类实现了Qt C++中QWidget的无边框窗口特性，其用法与QQuickFlatWindow类似，不再赘述。



