﻿/*!
    此控件依赖于QtFlatWindow模块：
    https://gitee.com/chen-jianli/qt-advanced-lib/tree/master/QtFlatWindow

    且要求注册QQuickFlatWindowHandler类到Qml，注册语句如下：
    qmlRegisterType<QQuickFlatWindowHandler>("Qt.FlatWindow", 1, 0, "QQuickFlatWindowHandler");
*/

import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import Qt.FlatWindow 1.0

Window{
    id:controlRoot

    property list<Item>    includItems
    property list<Item>    excludItems
    property var           excludRects

    property int    draggableHeight:titleBgHeight
    property int    draggableMargin:5
    property int    maximizedMargin:5

    property color  windowBgColor:"white"
    property color  windowBorderColor:titleBgColor
    property int    windowBorderWidth:1
    property bool   titleBgVisible:true
    property color  titleBgColor:"#0067EE"
    property int    titleBgHeight:40
    property color  titleButtonIconNormalColor:"white"
    property color  titleButtonIconHoveredColor:titleButtonIconNormalColor
    property color  titleButtonBgNormalColor:"transparent"
    property color  titleButtonBgHoveredColor:Qt.darker(titleBgColor,1.2)
    property int    titleButtonWidth:30
    property int    titleButtonHeight:30

    property bool   minimizeButtonVisible:true
    property bool   maximizeButtonVisble:true
    property bool   closeButtonVisible:true

    flags: Qt.Window | Qt.FramelessWindowHint
    width: 400
    height: 400

    Component.onCompleted: {

        for(var i1 in includItems){
            framelessHandler.addIncludeItem(includItems[i1])
        }

        for(var i2 in excludItems){
            framelessHandler.addExcludeItem(excludItems[i2])
        }

        var buttons = buttonComp.createObject(controlRoot)
        framelessHandler.addExcludeItem(buttons)

        for(var i3 in excludRects){
            framelessHandler.addExcludeRect(excludRects[i3])
        }

    }

    //无边框处理器
    QQuickFlatWindowHandler{
        id:framelessHandler
        topDraggableMargin: controlRoot.draggableMargin
        leftDraggableMargin: controlRoot.draggableMargin
        rightDraggableMargin: controlRoot.draggableMargin
        bottomDraggableMargin: controlRoot.draggableMargin
        topMaximizedMargin:  controlRoot.maximizedMargin
        bottomMaximizedMargin: controlRoot.maximizedMargin
        leftMaximizedMargin: controlRoot.maximizedMargin
        rightMaximizedMargin: controlRoot.maximizedMargin
        dragableHeight: controlRoot.draggableHeight
    }

    //窗口背景边框
    Rectangle {
        id: border
        anchors.fill: parent
        color:windowBgColor
        border.width: windowBorderWidth
        border.color: windowBorderColor
        visible: controlRoot.visible
    }

    //窗口标题栏背景
    Rectangle{
        id:titleBgRect
        y:windowBorderWidth
        visible: titleBgVisible
        width: controlRoot.width-controlRoot.windowBorderWidth*2
        height: titleBgHeight
        anchors.horizontalCenter: parent.horizontalCenter
        color: titleBgColor
    }

    //缩放按钮
    Component{
        id:buttonComp

        Row {
            id: buttonRow
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: windowBorderWidth
            height: titleButtonHeight
            z:3
            spacing: 0

            Button {
                id:minimizeButton
                visible: minimizeButtonVisible
                width: titleButtonWidth
                height: buttonRow.height
                display:Button.IconOnly
                icon.source:"/icon/minimize.svg"
                icon.width:height*0.6
                icon.height:height*0.6
                icon.color:minimizeButton.hovered?titleButtonIconHoveredColor:titleButtonIconNormalColor
                background: Rectangle{
                    color:minimizeButton.hovered?titleButtonBgHoveredColor:titleButtonBgNormalColor
                }
                onClicked: framelessHandler.minimizeWindow()
            }

            Button {
                id:maximizeButton
                visible: maximizeButtonVisble
                width: titleButtonWidth
                height: buttonRow.height
                display:Button.IconOnly
                icon.source:controlRoot.visibility===Window.Maximized?"/icon/normalmize.svg":"/icon/maximize.svg"
                icon.width:height*0.6
                icon.height:height*0.6
                icon.color:maximizeButton.hovered?titleButtonIconHoveredColor:titleButtonIconNormalColor
                background: Rectangle{
                    color:maximizeButton.hovered?titleButtonBgHoveredColor:titleButtonBgNormalColor
                }
                onClicked: framelessHandler.maximizeWindow()
            }

            Button {
                id:closeButton
                visible: closeButtonVisible
                width: titleButtonWidth
                height: buttonRow.height
                display:Button.IconOnly
                icon.source: "/icon/close.svg"
                icon.width:height*0.6
                icon.height:height*0.6
                icon.color:closeButton.hovered?titleButtonIconHoveredColor:titleButtonIconNormalColor
                background: Rectangle{
                    color:closeButton.hovered?titleButtonBgHoveredColor:titleButtonBgNormalColor
                }
                onClicked:{
                    framelessHandler.closeWindow()
                }
            }
        }
    }

    //关闭窗口。此函数为Window::close()的替代方法。请尽量使用此方法关闭窗口，
    //因为Window::close()会在用户将close.accepted设为true时失效。
    function closeWindow(){
        framelessHandler.closeWindow()
    }
}
