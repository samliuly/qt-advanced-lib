﻿import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.12

Window {
    id:root
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")


    Button{
        anchors.centerIn: parent
        text: "show window"
        onClicked: {
            flatWindow.show();
        }
    }


    AdFlatWindow{
        id:flatWindow

        onVisibleChanged: {
            if(visible){
                flatWindow.x=root.x+root.width/2-flatWindow.width/2
                flatWindow.y=root.y+root.height/2-flatWindow.height/2
            }
        }
    }
}
