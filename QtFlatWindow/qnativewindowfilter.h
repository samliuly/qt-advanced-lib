﻿#ifndef NATIVEWINDOWFILTER_H
#define NATIVEWINDOWFILTER_H

#include <QAbstractNativeEventFilter>

class QWindow;
class QNativeWindowHelper;

/*
    平台消息过滤器
*/
class QNativeWindowFilter : public QAbstractNativeEventFilter
{
public:
    static void deliver(QWindow *window, QNativeWindowHelper *helper);
protected:
    bool nativeEventFilter(const QByteArray &eventType,
                           void *message, long *result) final;
};

#endif // NATIVEWINDOWFILTER_H
