﻿#ifndef QWidgetFlatWindowHandler_H
#define QWidgetFlatWindowHandler_H

#include <QWidget>

class QWidgetFlatWindowHandlerPrivate;

/*
    QtWidget无边框窗口实现类
*/
class QWidgetFlatWindowHandler : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QWidgetFlatWindowHandler)

public:
    explicit QWidgetFlatWindowHandler(QWidget *parent = nullptr);
    virtual ~QWidgetFlatWindowHandler();

public:

    //设置可拖动区域
    QMargins draggableMargins() const;
    void setDraggableMargins(int left, int top, int right, int bottom);
    void setDraggableMargins(const QMargins &margins);
    QMargins maximizedMargins() const;
    void setMaximizedMargins(int left, int top, int right, int bottom);
    void setMaximizedMargins(const QMargins &margins);
    int titleBarHeight() const;
    void setTitleBarHeight(int value);
    qreal scaleFactor() const;
    bool isMaximized() const;

    //设置可响应窗口拖动的控件
    void addIncludeItem(QWidget *item);
    void removeIncludeItem(QWidget *item);
    void addExcludeItem(QWidget *item);
    void removeExcludeItem(QWidget *item);

    //窗口状态控制
    void minimizeWindow();
    void maximizeWindow();
    void closeWindow();

signals:
    void titleBarHeightChanged(int newValue);
    void scaleFactorChanged(qreal factor);
    void maximizedChanged(bool maximized);

protected:
    bool eventFilter(QObject *obj, QEvent *ev) final;

    QScopedPointer<QWidgetFlatWindowHandlerPrivate> d_ptr;
};

#endif // QWidgetFlatWindowHandler_H
