﻿#ifndef QWindowsPerformance_H
#define QWindowsPerformance_H

#include <QObject>
#include <QThread>
#include <QSet>
#include <QJsonObject>

/*
    Windows性能接口
*/
class QWindowsPerformance : public QThread
{
    Q_OBJECT

public:
    enum PerformanceType{
        CPUUsageRate=0,          //CPU占用率： %
        CPURunTime=1,            //CPU运行时间： s
        MemoryUsageRate=2,       //内存占用率：%
        MemoryUsageAmount=3,     //内存总用量：mb
        NetworkDownloadSpeed=4,  //下载速度：kb/s
        NetworkUploadSpeed=5,    //上传速度：kb/s
    };

public:
    explicit QWindowsPerformance(QObject *parent = nullptr);
    ~QWindowsPerformance();

    //同步获取性能
    double performance(PerformanceType type);

    //异步获取性能
    QSet<PerformanceType> monitoringPerformances();
    void addMonitoringPerformance(PerformanceType type);
    void clearMonitoringPerformance();

    //性能监控控制
    bool startMonitor();
    bool reStartMonitor();
    bool stopMonitor();

    //性能监控配置
    int  dataCollectionInterval();
    void setDataCollectionInterval(int interval=1000);

signals:
    void performanceReadReady(QJsonObject data);    //异步监控有可用数据时触发
    void errorOccurred(QString errorMsg);           //异步监控出现错误时触发

protected:
    void run() override;

private:
    bool runFlag;
    int m_dataCollectionInterval;
    QSet<PerformanceType> m_monitoringPerformances;
};

#endif // QWindowsPerformance_H
