## QtConfig

### 1 介绍
QtConfig模块主要提供了可用于记录C++和Qml日志的类QLogger，以及可用于操作JSON格式配置文件的类QConfiger。

### 2 使用

（1）将QtConfig模块的整个文件夹复制到项目的某个目录下。  

（2）在项目的.pro文件中使用下述命令导入模块的.pri文件：
```
# ../QtConfig/qtconfig.pri是相对.pro文件的路径
include(../QtConfig/qtconfig.pri)
```

（3）对项目执行qmake刷新文件依赖。

### 3 API
#### 3.1 QLogger
QLogger可用于将C++中使用qDebug()、qWarning()等宏打印的信息，以及Qml中使用console打印的信息统一输出到外部文件中。其基本用法如下：

```
//main.cpp
int main(){
    //....    

    //加载并配置QLogger类
    QLogger::load();
    QLogger::setLogFileFullPath(QStandardPaths::writableLocation(QStandardPaths::DesktopLocation)+ "/log.txt");
    QLogger::setMsgContextVisible(QLogger::AlwaysShow);

    //...
}

// C++中使用
qDebug()<<"C++ Debug Message";
qWarning()<<"C++ Warning Message";
qCritical()<<"C++ Critical Message";
qInfo()<<"C++ Info Message";


// Qml中使用
console.log("Qml Debug Message")
console.warn("Qml Warning Message")
console.error("Qml Critical Message")
console.info("Qml Info Message")

```

#### 3.2 QConfiger
QConfiger可用于读写基于JSON格式的配置文件，其基本用法如下：

```
QString path =QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
QConfiger configer(path);

QJsonObject value;
value["key1"] = "value1";
value["key2"] = "value2";

qDebug()<<"write app.json :"<<configer.writeConfigFile("app.json",value);
qDebug()<<"read app.json :"<<configer.readConfigFile("app.json");
qDebug()<<"rename app.json :"<<configer.renameConfigFile("app.json","app2.json");
qDebug()<<"remove app2.json :"<<configer.removeConfigFile("app2.json");

```