﻿#ifndef QLOGGER_H
#define QLOGGER_H

#include <QObject>
#include <QFlags>

/*
    日志输出器
*/
class QLogger: public QObject
{
    Q_OBJECT

public:
    enum MsgType{
        DebugMsg=0x0,
        WarningMsg=0x1,
        CriticalMsg=0x2,
        FatalMsg=0x3,
        InfoMsg=0x4,
        AllMsg=0x5,
    };

    enum MsgContextVisible{
        Auto=0,
        AlwaysShow=1,
        AlwaysClose=2,
    };

    Q_DECLARE_FLAGS(MsgTypes, MsgType)

public:
    static void load();
    static void unload();

    static MsgTypes msgTypes();
    static void setMsgTypes(MsgTypes types);

    static MsgContextVisible msgContextVisible();
    static void setMsgContextVisible(MsgContextVisible visible);

    static QString logFileFullPath();
    static void setLogFileFullPath(const QString& path);

private:
    QLogger(QObject *parent = nullptr);
    static void defaultMsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

private:
    static MsgTypes m_msgTypes;
    static MsgContextVisible m_msgContextVisible;
    static QString  m_logFileFullPath;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(QLogger::MsgTypes)

#endif // QLOGGER_H
