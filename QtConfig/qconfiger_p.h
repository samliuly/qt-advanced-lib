﻿#ifndef QCONFIGFILEPRIVATE_H
#define QCONFIGFILEPRIVATE_H

#include <QObject>
#include "qconfiger.h"

class QConfigerPrivate : public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(QConfiger)

public:
    explicit QConfigerPrivate(QObject *parent = nullptr);

    bool writeConfig(const QString& filePath,const QJsonDocument& doc);
    QJsonValue readConfig(const QString&  ilePath);
    bool renameConfig(const QString& filePath,const QString& newFileName);
    bool removeConfig(const QString& filePath);

public:
    QString m_baseDir;
    QConfiger *q_ptr;
};

#endif // QCONFIGFILEPRIVATE_H
