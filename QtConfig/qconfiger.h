﻿#ifndef QCONFIGFILE_H
#define QCONFIGFILE_H

#include <QObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>

class QConfigerPrivate;

/*
    基于JSON的通用配置文件读写器
*/
class QConfiger : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QConfiger)

public:
    explicit QConfiger(const QString&  baseDir="",QObject *parent = nullptr);
    ~QConfiger();

public slots:
    QString baseDir() const;
    void setBaseDir(const QString& dir);

    bool writeConfigFile(const QString&  filePath,QJsonObject value);
    bool writeConfigFile(const QString&  filePath,QJsonArray value);
    QJsonValue readConfigFile(const QString&  filePath);
    bool renameConfigFile(const QString&  filePath,const QString& newFileName);
    bool removeConfigFile(const QString&  filePath);

signals:
    void configFileAdded(const QString&  filePath);
    void configFileRemoved(const QString&  filePath);
    void configFileRenamed(const QString&  oldFilePath,const QString&  newFilePath);
    void configFileUpdated(const QString&  filePath);

private:
    QConfigerPrivate *d_ptr;
};

#endif // QCONFIGFILE_H
