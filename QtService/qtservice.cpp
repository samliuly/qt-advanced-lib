﻿#include <qt_windows.h>
#include "qtservice.h"


/*
    创建服务对象。

    serviceName：服务名称
    serviceDescription：服务描述
    parent：父对象、
*/
QtService::QtService(int &argc, char **argv, QString serviceName, QString serviceDescription, QObject *parent):
    QObject(parent),
    QtServiceBase(argc,argv,serviceName)
{
    setServiceDescription(serviceDescription);
    setStartupType(QtServiceController::AutoStartup);
}

/*
    返回服务程序所在路径。
*/
QString QtService::modulePath()
{
    wchar_t path[_MAX_PATH];
    ::GetModuleFileNameW( 0, path, sizeof(path) );
    return QString::fromUtf16((unsigned short*)path);
}

void QtService::createApplication(int &argc, char **argv)
{
    app = new QCoreApplication(argc, argv);
}

int QtService::executeApplication()
{
    return QCoreApplication::exec();
}

