## QtService

### 1 介绍
QtService是一个用于开发Windows服务程序的通用模块。Windows服务程序也称为Windows守护进程，它能够伴随主机自动启动，并能够在后台常驻运行。使用QtService模块，用户既可以通过继承QtService基类快速开发Windows服务程序，还可以通过调用QtSeviceController类开发服务控制程序。

### 2 使用
模块用法可参考Test/test.pro项目中的 "QtService Test"用例。以下介绍本模块的一般使用方法：  

（1）将QtService模块的整个文件夹复制到项目的某个目录下。  

（2）在项目的.pro文件中使用下述命令导入模块的.pri文件：
```
# ../QtService/qtservice.pri是相对.pro文件的路径
include(../QtService/qtservice.pri)
```
（3）继承QtService类，并覆盖其start()、stop()等虚函数接口，实现服务的业务功能。例如：
```
class MyService:public QtService
{
public:
    MyService(int &argc, char **argv,
              QString serviceName, QString serviceDescription = "",
              QObject *parent = nullptr){};
protected:
    void start() override{
        //服务启动时执行
    };
    void stop() override {
        //服务结束时执行
    };
};
```

（4）在main函数中创建QtService子类的对象，并调用其QtService::exec()。例如：
```
int main(int argc, char *argv[])
{
    MyService service(argc,argv,"MyService","MyService Demo");
    return service.exec();
}
```

（5）编译项目，在命令行中使用如下语句运行服务的可执行程序，以向系统注册和启动服务（假设编译生成的可执行程序名为service.exe）：
```
# 带参数i的运行命令用于注册服务
service.exe -i
# 不带参数的运行命令用于启动服务
service.exe 
```
除此之外，还可以使用QtService/bat/目录下的startService.cmd和stopService.cmd两个批处理脚本启动和停止服务，但注意使用前需要先将脚本中的“exePath”变量修改为实际的服务可执行程序的路径。

### 3 API
#### 3.1 QtService
此类封装了Windows服务程序的通用接口，用户可以继承它来开发服务程序。
#### 3.2 QtServiceController
此类封装了管理Windows服务程序的通用接口，用户可以组合它来开发服务控制程序。
