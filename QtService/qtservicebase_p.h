﻿#ifndef QTSERVICEBASEPRIVATE_H
#define QTSERVICEBASEPRIVATE_H

#include <QObject>
#include "qtservicecontroller.h"
#include "qtservicebase.h"

class QtServiceBasePrivate
{
    Q_DECLARE_PUBLIC(QtServiceBase)
public:

    QtServiceBasePrivate(const QString &name);
    ~QtServiceBasePrivate();

    void startService();
    int run(bool asService, const QStringList &argList);
    bool install(const QString &account, const QString &password);

    bool start();

    QString filePath() const;
    bool sysInit();
    void sysSetPath();
    void sysCleanup();

public:
    QStringList args;
    QString serviceDescription;
    QtServiceController::StartupType startupType;
    QtServiceBase::ServiceFlags serviceFlags;

    QtServiceBase *q_ptr;
    QtServiceController controller;

    class QtServiceSysPrivate *sysd;
    static class QtServiceBase *instance;
};
#endif // QTSERVICEBASEPRIVATE_H
