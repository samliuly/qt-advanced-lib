﻿#ifndef QTSERVICEBASE_H
#define QTSERVICEBASE_H

#include <QObject>
#include "qtservicecontroller.h"

class QtServiceBasePrivate;
class QtServiceSysPrivate;

/*
    Qt服务的实现类
*/
class  QtServiceBase
{
    Q_DECLARE_PRIVATE(QtServiceBase)
public:

    enum MessageType
    {
    Success = 0, Error, Warning, Information
    };

    enum ServiceFlag
    {
        Default = 0x00,
        CanBeSuspended = 0x01,
        CannotBeStopped = 0x02,
        NeedsStopOnShutdown = 0x04
    };

    Q_DECLARE_FLAGS(ServiceFlags, ServiceFlag)

    QtServiceBase(int argc, char **argv, const QString &name);
    virtual ~QtServiceBase();

    QString serviceName() const;

    QString serviceDescription() const;
    void setServiceDescription(const QString &description);

    QtServiceController::StartupType startupType() const;
    void setStartupType(QtServiceController::StartupType startupType);

    ServiceFlags serviceFlags() const;
    void setServiceFlags(ServiceFlags flags);

    int exec();

    void logMessage(const QString &message, MessageType type = Success,
                int id = 0, uint category = 0, const QByteArray &data = QByteArray());

    static QtServiceBase *instance();

protected:

    virtual void start() = 0;
    virtual void stop();
    virtual void pause();
    virtual void resume();
    virtual void processCommand(int code);

    virtual void createApplication(int &argc, char **argv) = 0;
    virtual int executeApplication() = 0;
private:

    friend class QtServiceSysPrivate;
    QtServiceBasePrivate *d_ptr;
};

#endif // QTSERVICEBASE_H
