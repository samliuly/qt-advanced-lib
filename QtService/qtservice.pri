QT +=core

# Must add the console module to output information to the console
# CONFIG += console

win32:LIBS += -luser32

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS += $$PWD\*.h
SOURCES += $$PWD\*.cpp
