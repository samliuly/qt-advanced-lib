﻿#ifndef QTSERVICECONTROLLER_H
#define QTSERVICECONTROLLER_H

#include <QObject>
#include <QStringList>

class QtServiceControllerPrivate;

/*
    Qt服务控制实现类
*/
class  QtServiceController
{
    Q_DECLARE_PRIVATE(QtServiceController)
public:
    enum StartupType
    {
        AutoStartup = 0, ManualStartup
    };

    QtServiceController(const QString &name);
    virtual ~QtServiceController();

    bool isInstalled() const;
    bool isRunning() const;

    QString serviceName() const;
    QString serviceDescription() const;
    StartupType startupType() const;
    QString serviceFilePath() const;

    static bool install(const QString &serviceFilePath, const QString &account = QString(),
                const QString &password = QString());
    bool uninstall();

    bool start(const QStringList &arguments);
    bool start();
    bool stop();
    bool pause();
    bool resume();
    bool sendCommand(int code);

private:
    QtServiceControllerPrivate *d_ptr;
};


#endif // QTSERVICECONTROLLER_H
