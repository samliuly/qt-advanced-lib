﻿#ifndef QTSERVICE_H
#define QTSERVICE_H

#include <QObject>
#include <QCoreApplication>
#include "qtservicebase.h"

/*
    Qt服务的简单包装类

    1. 如何使用
    QtService是QtServiceBase的简单包装类，其典型用法如下：
    （1）继承QtService，并覆盖其start()、stop()、 resume()、
    pause()、 processCommand()接口。
    （2）在主函数中窗口QtService的实例，并调用QtServiceBase::exec()
    启动服务事件循环。例如：

    int main(int argc, char *argv[])
    {
        MyService myService(argc, argv,"MyService","Service Demo");
        return myService.exec();
    }

    2. 控制命令
    可以通过命令行对服务程序进行控制，服务程序接受以下几个参数：
    （1）如果不带参数运行服务程序，则服务程序将被启动，并且QtServiceBase::start()将会被触发。
    （2）如果以-t参数运行服务程序，则服务程序将被结束，并且QtServiceBase::stop()将会被触发。
    （4）如果以-p参数运行服务程序，则服务程序将被暂停，并且QtServiceBase::pause()将会被触发。
    （5）如果以-r参数运行服务程序，则服务程序将被恢复，并且QtServiceBase::resume()将会被触发。
    （6）如果以-i参数运行服务程序，则会向系统注册服务程序。
    （7）如果以-u参数运行服务程序，则会向系统卸载服务程序。
    （8）如果以-v参数运行服务程序，则服务会向控制台输出自身运行状态。
    （9）如果以-c参数运行服务程序，则会向服务程序传递命令码，并且QtServiceBase::processCommand()将会被触发。
    （10）如果以-e参数运行服务程序，则服务程序将会以普通进程方式运行。

    3. 工作目录
    由于操作系统的限制，服务程序被限制在C:\Windows\System32目录工作，无法再通过调用一般
    的API（例如QStandardPaths）来获取系统目录，你可以通过以下几种方式来获取：
    （1）在安装程序时，将你需要访问的目录添加到系统的环境变量，并在程序运行时访问它们。
    （2）如果你需要获取程序所在目录，你可以调用modulePath()来获取。
*/
class QtService : public QObject, public QtServiceBase
{
    Q_OBJECT
public:
    explicit QtService(int &argc, char **argv,
                     QString serviceName, QString serviceDescription = "",
                     QObject *parent = nullptr);
    QString modulePath();

protected:
    virtual void createApplication(int &argc, char **argv) override;
    virtual int executeApplication() override;

private:
    QCoreApplication *app;
};

#endif // QTSERVICE_H
