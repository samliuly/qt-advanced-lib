﻿#ifndef QTSERVICECONTROLLERPRIVATE_H
#define QTSERVICECONTROLLERPRIVATE_H

#include "qtservicecontroller.h"

class QtServiceControllerPrivate
{
    Q_DECLARE_PUBLIC(QtServiceController)
public:
    QString serviceName;
    QtServiceController *q_ptr;
};

#endif // QTSERVICECONTROLLERPRIVATE_H
